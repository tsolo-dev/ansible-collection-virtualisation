# SPDX-FileCopyrightText: 2018-present Martin Slabber <martin@tsolo.io>
#
# SPDX-License-Identifier: Apache-2.0
__version__ = "1.0.0"
