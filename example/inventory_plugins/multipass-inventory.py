#!/usr/bin/env python3

# And example inventory_plugin for Ansible to get the inventory from multipass.
# This plugin makes assumptions based on the virtual machine names used.
#
# Add the file multipass.yml to your base ansible directory and reference this file as the hosts file.
# Example multipass.yml
#  ---
#  plugin: multipass-inventory
#  multipass:
#    user: ubuntu
#  compose:
#    ansible_user: ubuntu
#
# This Python file go into the inventory_plugin directory.
# Usage: `ansible-playbook -u multipass.yml site.yml`
#
# Snippets of code from all over. Credit to all the kind contributers on the internet.
import json
import re
import subprocess

from ansible.plugins.inventory import BaseInventoryPlugin

DOCUMENTATION = r"""
    name: multipass-inventory
    plugin_type: inventory
    short_description: Returns Ansible inventory from multipass
    description: Returns Ansible inventory from multipass
    options:
      plugin:
          description: Name of the plugin.
          required: true
          choices: ['multipass-inventory']
      compose:
          description: Create vars from jinja2 expressions.
          required: false
          default: {}
          type: dictionary
          """


class InventoryModule(BaseInventoryPlugin):
    NAME = "multipass-inventory"

    def verify_file(self, path):
        """return true/false if this is possibly a valid file for this plugin to consume"""
        valid = False
        if super().verify_file(path):
            if path.endswith(("multipass.yaml", "multipass.yml")):
                valid = True
        return valid

    def parse(self, inventory, loader, path, cache=True):
        super().parse(inventory, loader, path, cache)

        config = self._read_config_data(path)
        print(config)
        set_ansible_vars = {}
        multipass_config = config.get("multipass")
        if multipass_config:
            user = multipass_config.get("user")
            if user:
                set_ansible_vars["ansible_user"] = user

        vms = self.multipass_list()
        for vm in vms:
            name = vm["name"]
            if name == "primary":
                continue
            for group in self._name_to_groups(name):
                self.inventory.add_group(group)
                self.inventory.add_host(name, group=group)
            else:
                self.inventory.add_host(name)
            ipaddr = vm["ipv4"][0]
            self.inventory.set_variable(name, "ansible_host", ipaddr)
            for var_name, var_value in set_ansible_vars.items():
                self.inventory.set_variable(name, var_name, var_value)

    def _name_to_groups(self, name):
        groups = []
        for match in re.findall(r"([\w]*)-([a-zA-Z ]*)\d*", name):
            groups.extend(match)
            break
        if "-controller01" in name:
            groups.pop(groups.index("controller"))
            groups.append("initial_controller")
        return groups

    def multipass_list(self):
        multipass_vms = subprocess.run(["multipass", "list", "--format=json"], capture_output=True)
        vms = json.loads(multipass_vms.stdout)
        return vms["list"]
