# tsolo.virtualisation/libvirt

Manage the creation, and removal, of VMs using libvirt and cloudinit.

## Table of content

- [Default Variables](#default-variables)
  - [deploy_vm](#deploy_vm)
  - [destroy_vm](#destroy_vm)
  - [libvirt_pool_dir](#libvirt_pool_dir)
  - [ssh_key](#ssh_key)
  - [vm_image_filename](#vm_image_filename)
  - [vm_image_release_name](#vm_image_release_name)
  - [vm_image_url](#vm_image_url)
  - [vm_memory_size](#vm_memory_size)
  - [vm_network_bridge_device](#vm_network_bridge_device)
  - [vm_root_pass](#vm_root_pass)
  - [vm_root_size](#vm_root_size)
  - [vm_vcpu_count](#vm_vcpu_count)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### deploy_vm

If the VM needs to be deployed.

#### Default value

```YAML
deploy_vm: false
```

### destroy_vm

If VM should be removed/destroyed. This is done before VM is deployed.

#### Default value

```YAML
destroy_vm: false
```

### libvirt_pool_dir

Working directory to create VMs in.

#### Default value

```YAML
libvirt_pool_dir: /var/lib/libvirt/images
```

### ssh_key

SSH public key to add to the VM.

#### Default value

```YAML
ssh_key: /root/.ssh/id_rsa.pub
```

### vm_image_filename

The name of the image file. This is used when downloading and saving the image file. In general this value should not be set. Default is derived from vm_image_release_name.

#### Default value

```YAML
vm_image_filename: '{{ vm_image_release_name }}-server-cloudimg-amd64.img'
```

### vm_image_release_name

The Ubuntu name of the image we will use.

#### Default value

```YAML
vm_image_release_name: focal
```

### vm_image_url

The url where the image will be downloaded from. In general this value should not be set. Default is derived from vm_image_filename.

#### Default value

```YAML
vm_image_url: https://cloud-images.ubuntu.com/{{ vm_image_release_name }}/current/{{
  vm_image_filename }}
```

### vm_memory_size

Size of the VM memory. The size should be written with a unit e.g. 3G or 3GB. See Ansible human_to_bytes for all available units. Size is assumed to be giga bytes (GB) if no unit is supplied.

#### Default value

```YAML
vm_memory_size: 2GB
```

### vm_network_bridge_device

Applicable to hypervisor only. Network device to bridge to the VM.

#### Default value

```YAML
vm_network_bridge_device: ovs0
```

### vm_root_pass

root password of the VM.

#### Default value

```YAML
vm_root_pass: test123
```

### vm_root_size

Size of the VM root filesystem. The size should be written with a unit e.g. 40G or 40GB. See Ansible human_to_bytes for all available units. Size is assumed to be giga bytes (GB) if no unit is supplied.

#### Default value

```YAML
vm_root_size: 30GB
```

### vm_vcpu_count

Number of virtual CPUs to allocate to the VM.

#### Default value

```YAML
vm_vcpu_count: 2
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
