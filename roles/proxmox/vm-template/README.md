# tsolo.virtualisation/proxmox/vm-template

Create the VM template used by the proxmox/vm role

## Table of content

- [Default Variables](#default-variables)
  - [proxmox_vm_cloudimg_storage](#proxmox_vm_cloudimg_storage)
  - [proxmox_vm_storage](#proxmox_vm_storage)
  - [proxmox_vm_template_name_default](#proxmox_vm_template_name_default)
  - [proxmox_vm_template_storage](#proxmox_vm_template_storage)
  - [proxmox_vm_template_ubuntu_url_default](#proxmox_vm_template_ubuntu_url_default)
  - [proxmox_vm_templates](#proxmox_vm_templates)
  - [vm_root_size](#vm_root_size)
  - [vm_template_root_size](#vm_template_root_size)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### proxmox_vm_cloudimg_storage

Location to download cloudimg's to.

#### Default value

```YAML
proxmox_vm_cloudimg_storage: /var/lib/vz/images
```

### proxmox_vm_storage

The storage device to use for the virtual machine template disk.

### proxmox_vm_template_name_default

The name of the virtual machine template. This name itself should be a template.

#### Default value

```YAML
proxmox_vm_template_name_default: ubuntu-{{ proxmox_vm_template_ubuntu_release }}-cloudinit-template-{{
  inventory_hostname }}
```

### proxmox_vm_template_storage

#### Default value

```YAML
proxmox_vm_template_storage: local-lvm
```

### proxmox_vm_template_ubuntu_url_default

The URL of the cloud image.

#### Default value

```YAML
proxmox_vm_template_ubuntu_url_default: https://cloud-images.ubuntu.com/{{ proxmox_vm_template_ubuntu_release
  }}/current/{{ proxmox_vm_template_ubuntu_release }}-server-cloudimg-amd64.img
```

### proxmox_vm_templates

A list of templates that should be prepared on each Proxmox server. Each item in the list should have an id and a ubuntu_release field, e.g. {'id': 99999, 'ubuntu_release': 'xenial'}

#### Default value

```YAML
proxmox_vm_templates:
  - id: 99990
    ubuntu_release: jammy
  - id: 99991
    ubuntu_release: focal
```

### vm_root_size

Size of the virtual machine template root filesystem. The size should be written with a unit e.g. 40G or 40GB. See Ansible human_to_bytes for all available units. Size is assumed to be giga bytes (GB) if no unit is supplied.

### vm_template_root_size

#### Default value

```YAML
vm_template_root_size: 60GB
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
