# Roles


## tsolo.virtualisation/libvirt

Manage the creation, and removal, of VMs using libvirt and cloudinit.

### deploy_vm

If the VM needs to be deployed.

#### Default value:

```yaml
deploy_vm: false
```

### destroy_vm

If VM should be removed/destroyed. This is done before VM is deployed.

#### Default value:

```yaml
destroy_vm: false
```

### libvirt_pool_dir

Working directory to create VMs in.

#### Default value:

```yaml
libvirt_pool_dir: /var/lib/libvirt/images
```

### ssh_key

SSH public key to add to the VM.

#### Default value:

```yaml
ssh_key: /root/.ssh/id_rsa.pub
```

### vm_image_filename

The name of the image file. This is used when downloading and saving the
image file. In general this value should not be set. Default is derived from
vm_image_release_name.

#### Default value:

```yaml
vm_image_filename: '{{ vm_image_release_name }}-server-cloudimg-amd64.img'
```

### vm_image_release_name

The Ubuntu name of the image we will use.

#### Default value:

```yaml
vm_image_release_name: focal
```

### vm_image_url

The url where the image will be downloaded from. In general this value should
not be set. Default is derived from vm_image_filename.

#### Default value:

```yaml
vm_image_url: https://cloud-images.ubuntu.com/{{ vm_image_release_name }}/current/{{ vm_image_filename }}
```

### vm_memory_size

Size of the VM memory.
The size should be written with a unit e.g. 3G or 3GB.
See Ansible human_to_bytes for all available units.
Size is assumed to be giga bytes (GB) if no unit is supplied.

#### Default value:

```yaml
vm_memory_size: 2GB
```

### vm_network_bridge_device

Applicable to hypervisor only.
Network device to bridge to the VM.

#### Default value:

```yaml
vm_network_bridge_device: ovs0
```

### vm_root_pass

root password of the VM.

#### Default value:

```yaml
vm_root_pass: test123
```
This is a secret, the value of production and development is not shown.

### vm_root_size

Size of the VM root filesystem.
The size should be written with a unit e.g. 40G or 40GB.
See Ansible human_to_bytes for all available units.
Size is assumed to be giga bytes (GB) if no unit is supplied.

#### Default value:

```yaml
vm_root_size: 30GB
```

### vm_vcpu_count

Number of virtual CPUs to allocate to the VM.

#### Default value:

```yaml
vm_vcpu_count: 2
```

## tsolo.virtualisation/proxmox/vm

Create VM in Proxmox

### deploy_vm

If the VM needs to be deployed.

#### Default value:

```yaml
deploy_vm: false
```

### destroy_vm

If VM should be removed/destroyed. This is done before VM is deployed.

#### Default value:

```yaml
destroy_vm: false
```

### proxmox_vm_cloudimg_storage

Location to download cloudimg's to.

#### Default value:

```yaml
proxmox_vm_cloudimg_storage: /var/lib/vz/images
```

### proxmox_vm_storage

The storage device to use for the root disk of the VM.

#### Default value:

```yaml
proxmox_vm_storage: local-lvm
```

### proxmox_vm_template

Name of the template to use for the VM.
Template is created on each Proxmox server and the template name must match
the server you deploy too. Use the variable proxmox_server in the template
name. See role tsolo.virtualisation/vm-template for more details on template
creation.
Set the value to *cloudimg* to use a cloud image, template will not be used.
The variable proxmox_vm_ubuntu_cloudimg_url is then used to specify where
this image can be downloaded from.

#### Default value:

```yaml
proxmox_vm_template: ubuntu-{{ vm_ubuntu_release }}-cloudinit-template-{{ proxmox_server }}
```

### proxmox_vm_ubuntu_cloudimg_url

The URL of the cloud image. Only used when proxmox_vm_template='cloudimg'.

#### Default value:

```yaml
proxmox_vm_ubuntu_cloudimg_url: https://cloud-images.ubuntu.com/{{ vm_ubuntu_release }}/current/{{ vm_ubuntu_release }}-server-cloudimg-amd64.img
```

### vm_disks

Additional disks to add to the VM.
vm_disks:
  - storage: local-lvm
    size: 100

#### Default value:

```yaml
vm_disks: []
```

### vm_memory_size

Size of the VM memory.
The size should be written with a unit e.g. 3G or 3GB.
See Ansible human_to_bytes for all available units.
Size is assumed to be giga bytes (GB) if no unit is supplied.

#### Default value:

```yaml
vm_memory_size: 2GB
```

### vm_net_gateway

The gateway IP address to use for the VM.

#### Default value:

```yaml
vm_net_gateway: ''
```

### vm_net_ip

The IP address for the VM.
Do not change.

#### Default value:

```yaml
vm_net_ip: '{{ ansible_host }}'
```

### vm_net_mask

The number of bit set in the netmask.

#### Default value:

```yaml
vm_net_mask: 24
```

### vm_root_size

Size of the VM root filesystem.
The size should be written with a unit e.g. 40G or 40GB.
See Ansible human_to_bytes for all available units.
Size is assumed to be giga bytes (GB) if no unit is supplied.

#### Default value:

```yaml
vm_root_size: 30GB
```

### vm_ubuntu_release

The Ubuntu release codename. e.g. jammy, focal

#### Default value:

```yaml
vm_ubuntu_release: jammy
```

### vm_vcpu_count

Number of virtual CPUs to allocate to the VM.

#### Default value:

```yaml
vm_vcpu_count: 2
```

## tsolo.virtualisation/proxmox/vm-template

Create the VM template used by the proxmox/vm role

### proxmox_vm_cloudimg_storage

Location to download cloudimg's to.

#### Default value:

```yaml
proxmox_vm_cloudimg_storage: /var/lib/vz/images
```

### proxmox_vm_storage

The storage device to use for the virtual machine template disk.

### proxmox_vm_template_name_default

The name of the virtual machine template. This name itself should be a
template.

#### Default value:

```yaml
proxmox_vm_template_name_default: ubuntu-{{ proxmox_vm_template_ubuntu_release }}-cloudinit-template-{{ inventory_hostname }}
```

### proxmox_vm_template_storage


#### Default value:

```yaml
proxmox_vm_template_storage: local-lvm
```

### proxmox_vm_template_ubuntu_url_default

The URL of the cloud image.

#### Default value:

```yaml
proxmox_vm_template_ubuntu_url_default: https://cloud-images.ubuntu.com/{{ proxmox_vm_template_ubuntu_release }}/current/{{ proxmox_vm_template_ubuntu_release }}-server-cloudimg-amd64.img
```

### proxmox_vm_templates

A list of templates that should be prepared on each Proxmox server.
Each item in the list should have an id and a ubuntu_release field,
e.g. {'id': 99999, 'ubuntu_release': 'xenial'}

#### Default value:

```yaml
proxmox_vm_templates:
- id: 99990
  ubuntu_release: jammy
- id: 99991
  ubuntu_release: focal
```

### vm_root_size

Size of the virtual machine template root filesystem.
The size should be written with a unit e.g. 40G or 40GB.
See Ansible human_to_bytes for all available units.
Size is assumed to be giga bytes (GB) if no unit is supplied.

### vm_template_root_size


#### Default value:

```yaml
vm_template_root_size: 60GB
```