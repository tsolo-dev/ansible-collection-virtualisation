# tsolo.virtualisation/proxmox/vm

Create VM in Proxmox

## Table of content

- [Default Variables](#default-variables)
  - [deploy_vm](#deploy_vm)
  - [destroy_vm](#destroy_vm)
  - [proxmox_vm_cloudimg_storage](#proxmox_vm_cloudimg_storage)
  - [proxmox_vm_storage](#proxmox_vm_storage)
  - [proxmox_vm_storage_format](#proxmox_vm_storage_format)
  - [proxmox_vm_template](#proxmox_vm_template)
  - [proxmox_vm_ubuntu_cloudimg_url](#proxmox_vm_ubuntu_cloudimg_url)
  - [vm_disks](#vm_disks)
  - [vm_memory_size](#vm_memory_size)
  - [vm_net_gateway](#vm_net_gateway)
  - [vm_net_ip](#vm_net_ip)
  - [vm_net_mask](#vm_net_mask)
  - [vm_net_proxmox_interface](#vm_net_proxmox_interface)
  - [vm_net_vlan](#vm_net_vlan)
  - [vm_root_size](#vm_root_size)
  - [vm_ubuntu_release](#vm_ubuntu_release)
  - [vm_vcpu_count](#vm_vcpu_count)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### deploy_vm

If the VM needs to be deployed.

#### Default value

```YAML
deploy_vm: false
```

### destroy_vm

If VM should be removed/destroyed. This is done before VM is deployed.

#### Default value

```YAML
destroy_vm: false
```

### proxmox_vm_cloudimg_storage

Location to download cloudimg's to.

#### Default value

```YAML
proxmox_vm_cloudimg_storage: /var/lib/vz/images
```

### proxmox_vm_storage

The storage device to use for the root disk of the VM.

#### Default value

```YAML
proxmox_vm_storage: local-lvm
```

### proxmox_vm_storage_format

The storage device to use for the root disk of the VM.

#### Default value

```YAML
proxmox_vm_storage_format: qcow2
```

### proxmox_vm_template

Name of the template to use for the VM. Template is created on each Proxmox server and the template name must match the server you deploy too. Use the variable proxmox_server in the template name. See role tsolo.virtualisation/vm-template for more details on template creation. Set the value to *cloudimg* to use a cloud image, template will not be used. The variable proxmox_vm_ubuntu_cloudimg_url is then used to specify where this image can be downloaded from.

#### Default value

```YAML
proxmox_vm_template: ubuntu-{{ vm_ubuntu_release }}-cloudinit-template-{{ proxmox_server
  }}
```

### proxmox_vm_ubuntu_cloudimg_url

The URL of the cloud image. Only used when proxmox_vm_template='cloudimg'.

#### Default value

```YAML
proxmox_vm_ubuntu_cloudimg_url: https://cloud-images.ubuntu.com/{{ vm_ubuntu_release
  }}/current/{{ vm_ubuntu_release }}-server-cloudimg-amd64.img
```

### vm_disks

Additional disks to add to the VM. vm_disks: - storage: local-lvm size: 100 format: qcow2 or raw - Default to qcow2

#### Default value

```YAML
vm_disks: []
```

### vm_memory_size

Size of the VM memory. The size should be written with a unit e.g. 3G or 3GB. See Ansible human_to_bytes for all available units. Size is assumed to be giga bytes (GB) if no unit is supplied.

#### Default value

```YAML
vm_memory_size: 2GB
```

### vm_net_gateway

The gateway IP address to use for the VM.

#### Default value

```YAML
vm_net_gateway: ''
```

### vm_net_ip

The IP address for the VM. Do not change.

#### Default value

```YAML
vm_net_ip: '{{ ansible_host }}'
```

### vm_net_mask

The number of bit set in the netmask.

#### Default value

```YAML
vm_net_mask: 24
```

### vm_net_proxmox_interface

The interface on proxmox to use for the VM.

#### Default value

```YAML
vm_net_proxmox_interface: vmbr0
```

### vm_net_vlan

The VLAN ID to use for the interface, use ID=0 for untagged network.

#### Default value

```YAML
vm_net_vlan: 0
```

### vm_root_size

Size of the VM root filesystem. The size should be written with a unit e.g. 40G or 40GB. See Ansible human_to_bytes for all available units. Size is assumed to be giga bytes (GB) if no unit is supplied.

#### Default value

```YAML
vm_root_size: 30GB
```

### vm_ubuntu_release

The Ubuntu release codename. e.g. jammy, focal

#### Default value

```YAML
vm_ubuntu_release: jammy
```

### vm_vcpu_count

Number of virtual CPUs to allocate to the VM.

#### Default value

```YAML
vm_vcpu_count: 2
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
