# tsolo.virtualisation/proxmox/server

Configure Proxmox server

## Table of content

- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---



## Dependencies

- ordereddict([('role', 'tsolo.node.prometheus_exporters')])

## License

Apache-2.0

## Author

Tsolo.io
